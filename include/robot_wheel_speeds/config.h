
#ifndef _CONFIG_H_
#define _CONFIG_H_
//in our configuration
//   (A)//   x+     \\(B)
//
//   y+
//
//   (C)\\          //(D)

float PI = 3.14159265358979323846;
//float xn=0.08725;
//float yn=0.105;
float r=0.03;
float K = 0.08725 + 0.105; //m
int max_rmp=330;
float resolution=2*PI*r/1440.0;

#define constrain(amt,low,high) \
  ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

#endif