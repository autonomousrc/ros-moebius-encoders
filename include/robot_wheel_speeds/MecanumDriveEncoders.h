#pragma once

#include "robot_wheel_speeds/WheelEncoder.h"

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

#include <thread>
#include <atomic>
#include <mutex>

class MecanumDriveEncoders
{
public:
    MecanumDriveEncoders(int motorApinA, int motorApinB, int motorBpinA, int motorBpinB,
                         int motorCpinA, int motorCpinB, int motorDpinA, int motorDpinB,
                         int ticksPerRevolution, int wheelDiameterMm, int wheelAxisMm,
                         const ros::Duration& velocityUpdateInterval);
    ~MecanumDriveEncoders();

    void Start();
    nav_msgs::Odometry GetOdometry() const;

private:
    void calculateMecanumVelocites(float wA, float wB, float wC, float wD, geometry_msgs::Twist&) const;

    WheelEncoder m_A;
    WheelEncoder m_B;
    WheelEncoder m_C;
    WheelEncoder m_D;
    const float  m_wheelAxis; // spacing between wheels in metres

    geometry_msgs::Twist twist;
    geometry_msgs::Pose pose;

    float x; // Initial position on x plane
    float y; // Initial position on y plane
    float theta; // Initial rotation around z axis (yaw)

    float covariance_vector[6]; // Hold history of values to calculate covariance

    ros::Time          m_timeNow;
    std::thread        m_encoderPollingThread;
    std::atomic_bool   m_keepThreadAlive;
    mutable std::mutex m_mutex;
};
