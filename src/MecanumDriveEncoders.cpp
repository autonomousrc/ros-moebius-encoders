#include "robot_wheel_speeds/MecanumDriveEncoders.h"
#include "robot_wheel_speeds/config.h"
#include <tf/tf.h>

MecanumDriveEncoders::MecanumDriveEncoders(
        int motorApinA,
        int motorApinB,
        int motorBpinA,
        int motorBpinB,
        int motorCpinA,
        int motorCpinB,
        int motorDpinA,
        int motorDpinB,
        int ticksPerRevolution,
        int wheelDiameterMm,
        int wheelAxisMm,
        const ros::Duration& velocityUpdateInterval
    )
    : m_A(motorApinA, motorApinB, ticksPerRevolution, wheelDiameterMm, velocityUpdateInterval),
      m_B(motorBpinA, motorBpinB, ticksPerRevolution, wheelDiameterMm, velocityUpdateInterval),
      m_C(motorCpinA, motorCpinB, ticksPerRevolution, wheelDiameterMm, velocityUpdateInterval),
      m_D(motorDpinA, motorDpinB, ticksPerRevolution, wheelDiameterMm, velocityUpdateInterval),
      m_wheelAxis(float(wheelAxisMm)/1000),
      m_keepThreadAlive(false),
      twist(),
      pose(),
      x(0.0),
      y(0.0),
      theta(0.0),
      covariance_vector()
{
}

MecanumDriveEncoders::~MecanumDriveEncoders()
{
    m_keepThreadAlive = false;
    m_encoderPollingThread.join();
}

void MecanumDriveEncoders::Start()
{
    m_keepThreadAlive = true;
    m_encoderPollingThread = std::thread(
        [this]()
        {
            ROS_INFO("Starting polling thread");
            while(m_keepThreadAlive)
            {
                std::lock_guard<std::mutex> lock(m_mutex);

                // Trigger read from wheel encoders
                m_timeNow = ros::Time::now();
                m_A.read(m_timeNow);
                m_B.read(m_timeNow);
                m_C.read(m_timeNow);
                m_D.read(m_timeNow);

                // Populate twist
                calculateMecanumVelocites(
                    m_A.GetVelocity(),
                    m_B.GetVelocity(),
                    m_C.GetVelocity(),
                    m_D.GetVelocity(),
                    twist
                );

                // Calculate pose
                float vxi = twist.linear.x;
                float vyi = twist.linear.y;
                float omegai = twist.angular.z;

                float dt = m_A.GetDeltaT();
                x+=vxi*cos(theta)*dt-vyi*sin(theta)*dt;
                y+=vxi*sin(theta)*dt+vyi*cos(theta)*dt;
                theta+=omegai*dt;
                if(theta > 3.14)
                    theta=-3.14;

                // Populate pose
                pose.position.x = x;
                pose.position.y = y;
                pose.orientation = tf::createQuaternionMsgFromYaw(theta);
            }
        });
}


nav_msgs::Odometry MecanumDriveEncoders::GetOdometry() const
{
    std::lock_guard<std::mutex> lock(m_mutex);

    nav_msgs::Odometry odom;
    geometry_msgs::TwistWithCovariance twistWithCovariance;
    geometry_msgs::PoseWithCovariance poseWithCovariance;

    twistWithCovariance.twist = twist;
    twistWithCovariance.covariance = {0};

    poseWithCovariance.pose = pose;
    poseWithCovariance.covariance = {0};

    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";
    odom.child_frame_id = "base_link";
    odom.twist = twistWithCovariance;
    odom.pose = poseWithCovariance;

    ROS_DEBUG("GetVelocities { wA:%f, wB:%f, wC:%f, wD:%f; lin x: %f, lin y: %f ang: %f }",
              m_A.GetVelocity(), m_B.GetVelocity(), m_C.GetVelocity(), m_D.GetVelocity(), twist.linear.x, twist.linear.y, twist.angular.z);

    return odom;
}

void MecanumDriveEncoders::calculateMecanumVelocites(float wA, float wB, float wC, float wD, geometry_msgs::Twist& velocities) const
{
    velocities.linear.x = r/4.0*(wA+wB+wC+wD);
    velocities.linear.y = r/4.0*(+wA-wB-wC+wD);
    velocities.angular.z = r/(4.0*K)*(-wA+wB-wC+wD);
}
