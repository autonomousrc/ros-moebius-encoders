#include <ros/ros.h>
#include <tf/tf.h>
#include <nav_msgs/Odometry.h>

#include "robot_wheel_speeds/MecanumDriveEncoders.h"
#include "robot_wheel_speeds/SystemGpio.h"

int main(int argc, char *argv[])
{
    // Config
    int velocityUpdateIntervalNs = 2e7;
    ros::Duration velocityUpdateInterval(0, velocityUpdateIntervalNs);
    int motorApinA = 24;
    int motorApinB = 23;
    int motorBpinA = 25;
    int motorBpinB = 22;
    int motorCpinA = 4;
    int motorCpinB = 21;
    int motorDpinA = 17;
    int motorDpinB = 18;
    int ticksPerRevolution = 300;
    int wheelDiameterMm = 80;
    int wheelAxisMm = 70;


    ros::init(argc, argv, "moebius");
    SystemGPIO gpio({motorApinA, motorApinB, motorBpinA, motorBpinB, motorCpinA, motorCpinB, motorDpinA, motorDpinB});

    ros::NodeHandle nodeHandle;

    auto publisher = nodeHandle.advertise<nav_msgs::Odometry>("odom", 50);

    ros::Rate loop_rate(20);

    ROS_INFO("Starting wheel_speeds");
    MecanumDriveEncoders encoders(motorApinA, motorApinB, motorBpinA, motorBpinB, motorCpinA, motorCpinB, motorDpinA, motorDpinB,
                                       ticksPerRevolution, wheelDiameterMm, wheelAxisMm,
                                       velocityUpdateInterval);

    encoders.Start();
    ROS_INFO("Encoders started");
    int seq = 1;
    while(ros::ok())
    {
        // publish here
        auto msg = encoders.GetOdometry();
        msg.header.seq = seq++;

//        publisher.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}
